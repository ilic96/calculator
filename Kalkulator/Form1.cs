﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Form1 : Form
    {
        private decimal ResultValue = 0;
        private string Operation = "";
        private bool isOperationPerformed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonClick(object sender, EventArgs e)
        {
            if ((textBoxResult.Text == "0") || (isOperationPerformed))
            {
                textBoxResult.Clear();
            }
            isOperationPerformed = false;
            Button btn = (Button)sender; // kastovanje dugmeta
            textBoxResult.Text += btn.Text; // preuzimanje vrednosti kliknutog dugmeta
        }
        // metoda za biranje operacije sa brojevima
        private void OperatorClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (ResultValue != 0)
            {
                buttonResult.PerformClick();
                Operation = btn.Text;
                labelCurrentOperation.Text = ResultValue + " " + Operation;
                isOperationPerformed = true;
            }
            else
            {
                Operation = btn.Text;
                ResultValue = decimal.Parse(textBoxResult.Text);
                labelCurrentOperation.Text = ResultValue + " " + Operation;
                isOperationPerformed = true;
            }
            
        }
        // metoda za brisanje unosa
        private void ClearClick(object sender, EventArgs e)
        {
            textBoxResult.Text = "0";
        }
        // metoda za izracunavanje 
        private void EqualsClick(object sender, EventArgs e)
        {
            switch (Operation)
            {
                case "+":
                    textBoxResult.Text = (ResultValue + decimal.Parse(textBoxResult.Text)).ToString();
                    break;
                case "-":
                    textBoxResult.Text = (ResultValue - decimal.Parse(textBoxResult.Text)).ToString();
                    break;
                case "*":
                    textBoxResult.Text = (ResultValue * decimal.Parse(textBoxResult.Text)).ToString();
                    break;
                case "/":
                    textBoxResult.Text = (ResultValue / decimal.Parse(textBoxResult.Text)).ToString();
                    break;
                default:
                    break;
            }
            ResultValue = decimal.Parse(textBoxResult.Text);
            labelCurrentOperation.Text = "";

        }
    }
}
